﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektZPP.Models
{
    public interface IEfektRepository
    {
        IEnumerable<EfektKsztalcenia> EfektyKsztalcenia { get; }
    }
}
