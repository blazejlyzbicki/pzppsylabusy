﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektZPP.Models
{
    public enum Grupa
    {
        Wiedza,
        Umiejętnosci,
        [Display(Name = "Kompetencje Społeczne")]
        Kompetencje_społeczne

    }
}