﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjektZPP.Models
{
    
    public class EfektKsztalcenia
    {
        public int Id{ get; set; }
        public string Nazwa { get; set; }
        public string Opis { get; set; }
        public Grupa grupa { get; set; }
    }

}