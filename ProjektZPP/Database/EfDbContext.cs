﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ProjektZPP.Models;

namespace ProjektZPP.Database
{
    public class EfDbContext:DbContext
    {
        public DbSet<EfektKsztalcenia> Efekty { get; set; }
    }
}